//
//  UIScrollView+PIPreload.h
//  NewPiki
//
//  Created by Pham Quy on 1/28/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (PIPreload)
@property (nonatomic) BOOL enableDataPrefetch;
- (void) addPrefetchAction:(void(^)(void)) actionBlock
                  distance:(CGFloat) distance;
- (void) removePrefetchAction;
- (void) finishPrefetch;
@end
