//
//  UIScrollView+PIPreload.m
//  NewPiki
//
//  Created by Pham Quy on 1/28/15.
//  Copyright (c) 2015 Pikicast Inc. All rights reserved.
//

#import "UIScrollView+PIPreload.h"
#import "TransitionKit.h"
#import <objc/runtime.h>
#define SYNTHESIZE_CATEGORY_OBJ_PROPERTY(propertyGetter, propertySetter)                                                             \
- (id) propertyGetter {                                                                                                             \
return objc_getAssociatedObject(self, @selector( propertyGetter ));                                                             \
}                                                                                                                                   \
- (void) propertySetter (id)obj{                                                                                                    \
objc_setAssociatedObject(self, @selector( propertyGetter ), obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                            \
}


#define SYNTHESIZE_CATEGORY_VALUE_PROPERTY(valueType, propertyGetter, propertySetter)                                                \
- (valueType) propertyGetter {                                                                                                      \
valueType ret = {0};                                                                                                                  \
[objc_getAssociatedObject(self, @selector( propertyGetter )) getValue:&ret];                                                    \
return ret;                                                                                                                     \
}                                                                                                                                   \
- (void) propertySetter (valueType)value{                                                                                           \
NSValue *valueObj = [NSValue valueWithBytes:&value objCType:@encode(valueType)];                                                \
objc_setAssociatedObject(self, @selector( propertyGetter ), valueObj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);                       \
}

#define  _PIPrefetchStateNone          @"_PIPrefetchStateNone"
#define  _PIPrefetchStateTriggered     @"_PIPrefetchStateTriggered"


#define _PIPrefetchEventTrigger         @"_PIPrefetchEventTrigger"
#define _PIPrefetchEventDonePrefetch    @"_PIPrefetchEventDonePrefetch"


@interface _PIPrefetchController : NSObject
@property (nonatomic) CGFloat distance;
@property (nonatomic, copy) void(^actionBlock)();
@property (nonatomic) BOOL enable;
@property (nonatomic, weak) UIScrollView* scrollView;
@property (nonatomic, strong) TKStateMachine *stateMachine;

- (instancetype) initWithScrollView:(UIScrollView*) scrollView;
@end


@implementation _PIPrefetchController
- (instancetype) initWithScrollView:(UIScrollView*) scrollView
{
    self = [super init];
    if (self) {
        self.scrollView = scrollView;
        [self initStateMachine];
    }
    return self;

}


//------------------------------------------------------------------------------
- (void) initStateMachine
{
    TKStateMachine *stateMachine = [TKStateMachine new];
    
    TKState *initState = [TKState stateWithName:_PIPrefetchStateNone];
    [initState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        NSLog(@"%@--->%@", transition.sourceState.name, transition.destinationState.name);
    }];
    
    
    TKState *triggeredState = [TKState stateWithName:_PIPrefetchStateTriggered];
    [triggeredState setDidEnterStateBlock:^(TKState *state, TKTransition *transition) {
        NSLog(@"%@--->%@", transition.sourceState.name, transition.destinationState.name);
        if (self.actionBlock) {
            self.actionBlock();
        }
    }];

    [stateMachine addStates:@[ initState, triggeredState]];
    stateMachine.initialState = initState;

    TKEvent *triggerEvent = [TKEvent eventWithName:_PIPrefetchEventTrigger
                          transitioningFromStates:@[ initState ]
                                          toState:triggeredState];
    
    TKEvent *doneEvent = [TKEvent eventWithName:_PIPrefetchEventDonePrefetch
                           transitioningFromStates:@[ triggeredState ]
                                           toState:initState];

    [stateMachine addEvents:@[ triggerEvent, doneEvent]];

    // Activate the state machine
    [stateMachine activate];

    self.stateMachine = stateMachine;
}

//------------------------------------------------------------------------------
#pragma mark - Accessor
- (void) setScrollView:(UIScrollView *)scrollView
{
    _scrollView = scrollView;
}
//------------------------------------------------------------------------------
- (void) setEnable:(BOOL)enable
{
    NSAssert(_scrollView, @"No scroll view registered");
    if (self.enable != enable) {
        _enable = enable;
        if(enable)
        {
            [_scrollView addObserver:self
                          forKeyPath:@"contentOffset"
                             options:NSKeyValueObservingOptionNew|NSKeyValueObservingOptionOld
                             context:NULL];

        }else{
            [_scrollView removeObserver:self forKeyPath:@"contentOffset"];
        }
    }
    
}
//------------------------------------------------------------------------------
- (void) dealloc
{
    [self setEnable:NO];
}

//------------------------------------------------------------------------------
#pragma mark - Handle frame & size change
//------------------------------------------------------------------------------
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary *)change
                       context:(void *)context
{
    
    if ([keyPath isEqualToString:@"contentOffset"]) {
        [self handleContentOffsetChange:change];
    }

}

//------------------------------------------------------------------------------
- (void) handleContentOffsetChange:(NSDictionary*) change
{
    CGPoint new = [[change objectForKey:NSKeyValueChangeNewKey] CGPointValue];
    CGPoint old = [[change objectForKey:NSKeyValueChangeOldKey] CGPointValue];
    CGFloat movement = new.y - old.y;
    
    if (fabs(movement) < FLT_EPSILON) {
        return;
    }
    
    CGFloat distanceToBottom = _scrollView.contentSize.height - _scrollView.frame.size.height - new.y;
    BOOL canTrigger = (distanceToBottom - self.distance) < FLT_EPSILON;
    
    NSError* error = nil;
    if ([_stateMachine isInState:_PIPrefetchStateNone]) {
        if (canTrigger && (movement > 0)) {
            [self.stateMachine fireEvent:_PIPrefetchEventTrigger userInfo:nil error:&error];
        }
    }
    else if ([_stateMachine isInState:_PIPrefetchStateTriggered])
    {
        
    }
}

//------------------------------------------------------------------------------
- (void) finishPrefetch
{
    if ([self.stateMachine canFireEvent:_PIPrefetchEventDonePrefetch]) {
        [self.stateMachine fireEvent:_PIPrefetchEventDonePrefetch userInfo:nil error:nil];
    }
}
@end

//------------------------------------------------------------------------------
//------------------------------------------------------------------------------
@interface UIScrollView ()
@property (nonatomic, strong) _PIPrefetchController* pikiScrollviewPrefetchController;
@end

@implementation UIScrollView (PIPreload)
SYNTHESIZE_CATEGORY_OBJ_PROPERTY(pikiScrollviewPrefetchController, setPikiScrollviewPrefetchController:)

- (void) addPrefetchAction:(void(^)(void)) actionBlock
                  distance:(CGFloat) distance
{
    _PIPrefetchController* controller = [[_PIPrefetchController alloc] initWithScrollView:self];
    controller.distance = distance;
    controller.actionBlock = actionBlock;
    [self setPikiScrollviewPrefetchController:controller];
    self.pikiScrollviewPrefetchController.enable = YES;
}

//------------------------------------------------------------------------------
//- (void)willMoveToSuperview:(UIView *)newSuperview
//{
//    if (self.superview && newSuperview == nil) {
//        if (self.pikiScrollviewPrefetchController)
//        {
//            [self.pikiScrollviewPrefetchController setEnable:NO];
//        }
//    }
//}
//------------------------------------------------------------------------------
- (void) setEnableDataPrefetch:(BOOL)enableDataPrefetch
{
    [self.pikiScrollviewPrefetchController setEnable:enableDataPrefetch];
}
//------------------------------------------------------------------------------
- (BOOL) enableDataPrefetch
{
    return self.pikiScrollviewPrefetchController.enable;
}

//------------------------------------------------------------------------------
- (void) finishPrefetch
{
    [self.pikiScrollviewPrefetchController finishPrefetch];
}

//------------------------------------------------------------------------------
- (void) removePrefetchAction
{
    if (self.pikiScrollviewPrefetchController)
    {
        [self.pikiScrollviewPrefetchController setEnable:NO];
        [self setPikiScrollviewPrefetchController:nil];
    }
}
@end
