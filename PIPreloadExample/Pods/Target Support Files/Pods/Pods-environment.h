
// To check if a library is compiled with CocoaPods you
// can use the `COCOAPODS` macro definition which is
// defined in the xcconfigs so it is available in
// headers also when they are imported in the client
// project.


// PIPreload
#define COCOAPODS_POD_AVAILABLE_PIPreload
#define COCOAPODS_VERSION_MAJOR_PIPreload 0
#define COCOAPODS_VERSION_MINOR_PIPreload 0
#define COCOAPODS_VERSION_PATCH_PIPreload 1

// TransitionKit
#define COCOAPODS_POD_AVAILABLE_TransitionKit
#define COCOAPODS_VERSION_MAJOR_TransitionKit 2
#define COCOAPODS_VERSION_MINOR_TransitionKit 1
#define COCOAPODS_VERSION_PATCH_TransitionKit 1

