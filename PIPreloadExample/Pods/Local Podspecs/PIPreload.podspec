Pod::Spec.new do |s|

# Root specification

  s.name         = "PIPreload"
  s.version      = "0.0.1"
  s.summary      = "UIScrollView category for preload data as user scroll to the bottom. Easy use"
  s.homepage     = ""
  s.author       = { "Jack" => "psyquy@gmail.com" }
  s.source       = {:git => 'https://github.com/phamquy/UIScrollView-PIPreload.git'}
  
# Build setting
  s.dependency 'TransitionKit', '~> 2.1'
  s.ios.deployment_target = '7.0'
  s.platform = :ios, '7.0' 
  s.framework  = 'UIKit'
  s.requires_arc = true
  s.public_header_files = 'PIPreload/*.h'
  s.source_files  = 'PIPreload/**/*.{h,m}'
end