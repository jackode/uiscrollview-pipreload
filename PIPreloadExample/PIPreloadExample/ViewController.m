//
//  ViewController.m
//  PIPreloadExample
//
//  Created by Pham Quy on 2/5/15.
//  Copyright (c) 2015 Jkorp. All rights reserved.
//

#import "ViewController.h"
#import <PIPreload/UIScrollView+PIPreload.h>
@interface ViewController ()<UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UITableView *scrollview;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [_scrollview setBackgroundColor:[UIColor whiteColor]];
    [_scrollview setRowHeight:100];
    [_scrollview registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellId"];
    _scrollview.dataSource = self;

    __weak typeof(self) wself = self;
    [_scrollview
     addPrefetchAction:^{
         NSLog(@"Do prefetching");
         [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
         [wself performSelector:@selector(didPreLoad) withObject:nil afterDelay:3];
     } distance:70];
    

}

- (void) dealloc
{
    [_scrollview removePrefetchAction];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void) didPreLoad
{
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    [self.scrollview finishPrefetch];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 10;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell* cell  = [tableView dequeueReusableCellWithIdentifier:@"cellId" forIndexPath:indexPath];
    CGFloat hue = ( arc4random() % 256 / 256.0 );  //  0.0 to 1.0
    CGFloat saturation = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from white
    CGFloat brightness = ( arc4random() % 128 / 256.0 ) + 0.5;  //  0.5 to 1.0, away from black
    UIColor *color = [UIColor colorWithHue:hue saturation:saturation brightness:brightness alpha:1];
    [cell.contentView setBackgroundColor:color];
    return cell;
}
@end
